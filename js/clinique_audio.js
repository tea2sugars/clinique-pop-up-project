/*
Uses: http://jplayer.org/

To use this, first add the below into the head of your HTML (only really goes onto index.html)
<!-- AUDIO PLAYER -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="/js/jquery.jplayer.fade.min.js"></script>
<script type="text/javascript" src="/js/clinique_audio.js"></script>

Then add this into the body somewhere (Again, only really on index.html)
<div id="jquery_jplayer_parent" class="jp-jplayer"></div>

To then change the audio, at the top of your page, put in something along the lines of the below.  If you dont want a fade, just add a - after the audio filename in the variables
<!-- AUDIO PLAYER -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script>
  $(document).ready(function(){
    parent.switchAudio('UVA_UVB');
  });
</script>
*/

var currentTrack;

  /**
   * Function to change the playrate of the audio file
   *   http://jplayer.org/latest/developer-guide/#jPlayer-option-playbackRate
   * 
   * @param int amount
   *   - pass in a positive/negative number to alter the speed (ie 1 or -1)
   */
  function speedChange(amount) {
    // Set default value for amount, and normalise if needed
    amount = typeof amount !== 'undefined' ? amount : 1;

    //amount = amount / 2.5;
    if (amount < 0.2) {
      newPlaybackRate = 0.2;
    } else if (amount > 5) {
      newPlaybackRate = 5;
    } else {
      newPlaybackRate = amount;
    }

    // Get/set playback rates
    $("#jquery_jplayer_parent").jPlayer( "playbackRate", newPlaybackRate);
  }

  /**
   * Change the audio by fading the current piece out, then fading the new one in
   */
  function switchAudio(filename, duration) {
    filename = typeof filename !== 'undefined' ? filename : 'pollution';
    duration = typeof duration !== 'undefined' ? duration : 1000;
    filenameFormatted = filename.toUpperCase().replace("-", "_");
    // Fade out the audio
    $("#jquery_jplayer_parent").jPlayerFade().out(duration, null, null, function(){
      // Change the audio file and fade it in
      $(this).jPlayer("setMedia", {
        mp3: "/mp3/" + filenameFormatted + ".mp3",
      }).jPlayerFade().in(duration).jPlayer("play")
    }).jPlayer("play");
    currentTrack = filenameFormatted;
  }


  function initializeAudio(defaultAudio) {
    defaultAudio = typeof defaultAudio !== 'undefined' ? defaultAudio : 'pollution';
    
    // Instantiate the audio player, and get it to start playing
    $("#jquery_jplayer_1, #jquery_jplayer_parent").jPlayer({
      ready: function () {
        $(this).jPlayer("setMedia", {
          mp3: "/mp3/" + defaultAudio + ".mp3",
          minPlaybackRate: 0.1,
          maxPlaybackRate: 6,
          playbackRate: 1,
        }).jPlayer("play");
      },
      loop: true,
      volume: 1,
      supplied: "mp3",
    });
    currentTrack = defaultAudio;
    
  }