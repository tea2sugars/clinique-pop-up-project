$(document).ready(function(){
  // Default all containers to hidden (as they will fade in on load)
  $('#container').hide();
  $('#container').fadeIn(1000);

  $('.click-fadeout').on('click', function(event){
    destination = $(this).attr('href');
    fadeOutLink(destination)
    event.preventDefault();
  });

  $('.nav-back').on('click', function(event){
    destination = document.referrer;
    fadeOutLink(destination);
    event.preventDefault();
  });

  $('form input.nav-continue').on('click', function(event) {
    fadeOutSubmit($(this).closest('form'));
    event.preventDefault();
  });

  redirectHome();

});


function fadeOutLink(destination) {
  $('#container').fadeOut(400, function(){
    window.location.href = destination;
  });
}
function fadeOutSubmit(form) {
  $('#container').fadeOut(400, function(){
    form.submit();
  });
}
function redirectHome(){
  setInterval(function () {
    $(document).fadeOut();
  document.location.href  = document.location.href
  }, 60000);
}